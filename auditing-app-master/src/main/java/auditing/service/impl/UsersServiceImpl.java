package auditing.service.impl;

import auditing.dto.UsersDTO;
import auditing.exception.RoleNotFoundException;
import auditing.exception.UserNotFoundException;
import auditing.exception.UsersNotFoundIdException;
import auditing.mappers.RoleMapper;
import auditing.mappers.UserMapper;
import auditing.model.Role;
import auditing.model.Users;
import auditing.repository.RoleRepository;
import auditing.repository.UsersRepository;
import auditing.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private RoleRepository roleRepository;

    private  Users users;

    @Autowired
    @Lazy
    private PasswordEncoder passwordEncoder;

    @Override
    public List  listUsers() {
        Iterable<Users> allUsers = usersRepository.findAll ();
        return Arrays.asList (allUsers);
    }

    @Override
    public UsersDTO getUsers(Long id)  {
        Users  byId = usersRepository.findById (id)
                .orElseThrow(()-> new UserNotFoundException("User not found"));
        return UserMapper.INSTANCE.toDTO(byId);
    }

    @Override
    public UsersDTO createUsers(UsersDTO usersDTO) throws IllegalArgumentException {
        if (usersDTO.getId () != null){
            throw  new IllegalArgumentException("Users id must be null");
        }
        Role role = roleRepository.findById(usersDTO.getRole().getId()).orElseThrow(() -> new IllegalStateException("role not found"));
        usersDTO.setRole(RoleMapper.INSTANCE.roleToDTO(role));
        usersDTO.setPassword(passwordEncoder.encode(usersDTO.getPassword()));
        Users user = UserMapper.INSTANCE.users(usersDTO);
        System.out.println("userr"+user.getPassword());
        return UserMapper.INSTANCE.toDTO(usersRepository.save(user));
    }



    @Override
    public UsersDTO updateUsers(UsersDTO usersDTO, Long id) {
        usersDTO.setId (id);
        usersDTO.setPassword( passwordEncoder.encode(usersDTO.getPassword()));
        Users users = UserMapper.INSTANCE.users(usersDTO);
        return UserMapper.INSTANCE.toDTO(usersRepository.save (users));
    }



    @Override
    public UsersDTO deleteUsers(Long id) throws IllegalArgumentException{
        Optional<Users> byId = usersRepository.findById(id);
        if (byId.isPresent ()){
            usersRepository.deleteById(id);
        }else {
            // System.out.println ("No users with id:" + id);
            throw  new IllegalArgumentException("No users with id");
            // throw new UsersNotFoundIdException("No users with id");
        }
        Users usersDTO = byId.orElseThrow(NoSuchElementException::new);
        return UserMapper.INSTANCE.toDTO(usersDTO);
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Users user = usersRepository.findByUsername(s)
                .orElseThrow(() -> new UsernameNotFoundException("Invalid username"));
        return  user;
    }

    public Role findRoleWithName(String name) {
        Role role = roleRepository.findByName(name).orElseThrow(() ->
                new RoleNotFoundException(name));
        return role;
    }
}