<?php include_once "./includes/header.php"; ?>

<section id='home' class='home'>
    <div class='overlay'>
        <div class='container'>
            <div class='row'>
                <div class='col-sm-12 '>
                    <div class='main_home_slider text-center'>
                        <div class='single_home_slider'>
                            <div class='main_home wow fadeInUp' data-wow-duration='700ms'>
                                <h1>XİDMƏTLƏR</h1>
                                <p>Sayta daxil olmaqla aşağıdakı xidmətlərdən yararlana bilərsiniz</p>
                            </div>
                        </div>
                        <div class='single_home_slider'>
                            <div class='main_home wow fadeInUp' data-wow-duration='700ms'>
                                <h1>HELPDESK MÜRACİƏT</h1>
                                <p>Komputerlərinizdə yaranmış problemlərin həlli üçün müraciət forması</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<section id='service' class='service'>
    <div class='container'>
        <div class='row'>
            <div class='col-sm-12'>
                <div class='main_service_area'>
                    <div class='main_service_content'>
                        <div class='service_tabe'>
                            <!-- Nav tabs -->
                            <ul class='service_tabe_menu nav nav-tabs' role='tablist'>
                                <li role='presentation' class='active'>
                                    <a href='#webdesign' aria-controls='webdesign' role='tab' data-toggle='tab'>
                                        <br />İNSAN RESURSLARI</a>
                                </li>
                                <li role='presentation'>
                                    <a href='#appdesign' aria-controls='appdesign' role='tab' data-toggle='tab'>
                                        <br />MALİYYƏ ŞÖBƏSİ</a>
                                </li>
                                <li role='presentation'>
                                    <a href='#graphicdesign' aria-controls='graphicdesign' role='tab' data-toggle='tab'>
                                        <br />HÜQUQ ŞÖBƏSİ</a>
                                </li>
                                <li role='presentation'>
                                    <a href='#gamedesign' aria-controls='gamedesign' role='tab' data-toggle='tab'>BEYNƏLXALQ VƏ
                                        <br>İCTİMAİYYƏTLƏ
                                        <br>ƏLAQƏLƏR ŞÖBƏSİ</a>
                                </li>
                                <li role='presentation'>
                                    <a href='#gamedesign2' aria-controls='gaggvbmedesign2' role='tab' data-toggle='tab'>
                                        </i>
                                        <br />ÜMUMİ ŞÖBƏ</a>
                                </li>
                                <li role='presentation'>
                                    <a href='#gamedesign3' aria-controls='gamedesign3' role='tab' data-toggle='tab'>
                                        </i>İNFORMASİYA
                                        <br> TEXNOLOGİYALARI
                                        <br>ŞÖBƏSİ</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class='tab-content'>
                                <div role='tabpanel' class='tab-pane active' id='webdesign'>
                                    <div class='row' style='height: 400px;'>
                                        <div class='col-md-8' style='font-size: 14px; color: #000; font-family: Arial, Helvetica, sans-serif;'>
                                            <div class='media accordion-inner'>
                                                <div class='pull-left'>
                                                    <img src='img/esd.PNG'>
                                                </div>
                                                <div class='media-body'>

                                                    <p style='font-size: 14px; color: #000; font-family: Arial, Helvetica, sans-serif; margin-top: -7px;'>

                                                        <span style='font-size: 12px;'>09.04.2018</span>
                                                        <br> Hökümət Ödəniş Portalına Inteqrasiya

                                                        <br>ASDİS sənəd dövriyyəsi sistemi dövlət qurumları üçün nəzərdə tutulub.  Bu sistem Azərbaycan Respublikasının bütün dövlət idarələrinin iş və biznes proseslərinin xüsusiyyətlərini özündə əks etdirməklə yanaşı, həm də milli qanunvericilikdəki dəyişikliklərə asanlıqla adaptasiya olur. ASDİS sənəd dövriyyəsi siteminin əsas özəllikləri aşağıdakılardır:
                                                        <br><br>
                                                        sənəd üzərində kollektiv şəkildə işləmək imkanı (bu, kağız sənədlərlə iş zamanı mümkünsüzdür)<br>
                                                        sənəd axtarışı və seçimi prosesinin əhəmiyyətli dərəcədə sürətlənməsi;<br><br>
                                                        informasiya təhlükəsizliyinin yüksək səviyyədə təminatı; (elektron sənəd dövriyyəsinin tətbiqi zamanı hər kəs yalnız öz səlahiyyətləri daxilində olan məlumatları əldə edə bilər);<br><br>
                                                        sənədlərin daha uzun müddət və daha rahat şəkildə qorunub saxlanılması;<br><br>
                                                        sənədlərin icrasına nəzarətin güclənməsi.<br>
                                                        ASDİS sənəd dövriyyəsinin effektli şəkildə tətbiqinin nəticələri: əmək məhsuldarlığının qalxması, idarəçilik səviyyəsinin yüksəlişi, əməkdaşların məsuliyyətinin artması. Əməkdaşlar aktual informasiyalarla təmin olunur, müştərilərə təklif olunan xidmətlərin səviyyəsi yüksəlir. Eyni zamanda daxili xərclər və risklər minimuma endirilir, kommersiya sirrinin etibarlı qorunub saxlanılması təmin olunur.







                                                    </p>
                                                </div>
                                            </div>
                                            <div class='col-md-6'>

                                            </div>
                                        </div>


                                        <div class='col-sm-4'>
                                            <a href='#' style='font-size: 14px; font-family: Arial, Helvetica, sans-serif;'>İnformasiya təhlükəsizliyi</a><br>
                                            <a href='esd.html' style='font-size: 14px; font-family: Arial, Helvetica, sans-serif;'>Hökümət Ödəniş Portalına Inteqrasiya</a>
                                        </div>
                                    </div>
                                </div>






                                <div role='tabpanel' class='tab-pane' id='appdesign'>
                                    <div class='single_service_tab'>
                                        <div class='row'>

                                            <div class='col-sm-8'>
                                                <div class='single_tab_content'>

                                                </div>
                                            </div>


                                            <div class='col-sm-4'>
                                                <a href='#' style='font-size: 14px; font-family: Arial, Helvetica, sans-serif;'>Büdcə qanunvericiliyi</a><br>
                                                <a href='#' style='font-size: 14px; font-family: Arial, Helvetica, sans-serif;'>Hesabat</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div role='tabpanel' class='tab-pane' id='graphicdesign'>
                                    <div class='single_service_tab'>
                                        <div class='row'>

                                            <div class='col-sm-8'>
                                                <div class='single_tab_content'>

                                                </div>
                                            </div>

                                            <div class='col-sm-4'>
                                                <a href='doc/qanun.pdf' target='_blank' style='font-size: 14px; font-family: Arial, Helvetica, sans-serif;'>Qanunvericilik</a><br>
                                                <a href='#' style='font-size: 14px; font-family: Arial, Helvetica, sans-serif;'>Xəbərlər</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div role='tabpanel' class='tab-pane' id='gamedesign'>
                                    <div class='single_service_tab'>
                                        <div class='row'>

                                            <div class='col-sm-8'>
                                                <div class='single_tab_content'>

                                                </div>
                                            </div>

                                            <div class='col-sm-4'>
                                                <a href='#' style='font-size: 14px; font-family: Arial, Helvetica, sans-serif;'>Mətbuat xidməti</a><br>
                                                <a href='#' style='font-size: 14px; font-family: Arial, Helvetica, sans-serif;'>Hesabatlar</a><br>
                                                <a href='#' style='font-size: 14px; font-family: Arial, Helvetica, sans-serif;'>Xəbərlər</a>

                                            </div>

                                            <!--                                                        <div id='pentagon'></div>-->
                                        </div>
                                    </div>
                                </div>

                                <div role='tabpanel' class='tab-pane' id='gamedesign2'>
                                    <div class='single_service_tab'>
                                        <div class='row'>

                                            <div class='col-sm-8'>
                                                <div class='single_tab_content'>

                                                </div>
                                            </div>
                                            <div class='col-sm-4'>
                                                <a href='doc/esd.pdf' target='_blank' style='font-size: 14px; font-family: Arial, Helvetica, sans-serif;'>ESD haqqında təlimat</a><br>
                                                <a href='#' style='font-size: 14px; font-family: Arial, Helvetica, sans-serif;'>Xəbərlər</a>
                                            </div>

                                            <!--                                                        <div id='pentagon'></div>-->
                                        </div>
                                    </div>
                                </div>

                                <div role='tabpanel' class='tab-pane' id='gamedesign3'>
                                    <div class='single_service_tab'>
                                        <div class='row'>

                                            <div class='col-sm-8'>
                                                <div class='single_tab_content'>
                                                    <div class='media accordion-inner'>
                                                        <div class='pull-left'>
                                                            <img src='img/komp.PNG'>
                                                        </div>
                                                        <div class='media-body'>

                                                            <p style='font-size: 14px; color: #000; font-family: Arial, Helvetica, sans-serif; margin-top: -7px;'>

                                                                <span style='font-size: 12px;'>09.04.2018</span>
                                                                <br> İnformasiya təhlükəsizliyi haqqında təlimat
                                                                <a href='doc/Kompüter təhlükəsizliyi.pdf' target='_blank'>(keçid-PDF)</a>
                                                                <br> Çox təəssüf ki kompüterlərin məlumatsız və diqqətsiz
                                                                istifadəsi maddi və mənəvi zərərlərlə nəticələnir.
                                                                Bu zərərlərdən qaçmaq üçün bəzi təməl mövzuları
                                                                bilmək və bəzi təhlükəsizlik tədbirlərini almaq
                                                                lazımdır.
                                                                <br> Risklər Hardan Gələbilər? Kompüter istifadəsindən
                                                                qaynaqlanan risklər, müxtəlif şəkillərdə ortaya
                                                                çıxar. Işlətdiyimiz proqramlarda tapılması olabiləcək
                                                                açıqlar və səhvlər, zərər vermə məqsədiylə yazılmış
                                                                virus və bənzəri proqramları, pis niyyətli kəslərin
                                                                edə biləcəyi birbaşa və dolayı hücumları, yalan
                                                                cəhdləri və istifadəçi səhvləri bunlara örnəkdir
                                                                <br> Necə qoruna bilərsiniz?
                                                                <br> Əməliyyat sistemlərində və proqramlarda kompyüterinizə
                                                                zərər verəcək açıqlar və səhvlər ola bilər. Bu
                                                                açıqlar, pis niyyətli kəslər tərəfindən tapılsa
                                                                kompüterinizin yavaşlaması, səhv verməsi, istəmədiyiniz
                                                                tab-toggler tag_a active_menu default-sub-menu activeme                      şeylər etməsi, şəxsi məlumatlarınızın oğurlanma,
                                                                məlumat itkiləri kimi istənməyən nəticələr meydana
                                                                gələ bilər.
                                                                <a href='doc/Kompüter təhlükəsizliyi.pdf'
                                                                   target='_blank'>Ətraflı</a>





                                                            </p>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class='col-sm-4'>
                                                <a href='#' style='font-size: 14px; font-family: Arial, Helvetica, sans-serif;'>İnformasiya təhlükəsizliyi</a><br>
                                                <a href='esd.html' style='font-size: 14px; font-family: Arial, Helvetica, sans-serif;'>Hökümət Ödəniş Portalına Inteqrasiya</a>
                                            </div>


                                            <!--                                                        <div id='pentagon'></div>-->
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include_once "./includes/footer.php"; ?>