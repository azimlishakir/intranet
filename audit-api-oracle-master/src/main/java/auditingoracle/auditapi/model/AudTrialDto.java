package auditingoracle.auditapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AudTrialDto {
    private String  auditType;
    private String  sesionId ;
    private String  proxy_sesionId;
    private String  os_userName;
    private String  userHost;
    private String  actian_name;
    private String  dbUserName;
    private String  sql_Text;

    private String client_program_name;
    private String event_timestamp;
    private String object_name ;
    private String authentication_type;
    private String currentyDate;

    public String getAuditType() {
        return auditType;
    }

    public void setAuditType(String auditType) {
        this.auditType = auditType;
    }

    public String getSesionId() {
        return sesionId;
    }

    public void setSesionId(String sesionId) {
        this.sesionId = sesionId;
    }

    public String getProxy_sesionId() {
        return proxy_sesionId;
    }

    public void setProxy_sesionId(String proxy_sesionId) {
        this.proxy_sesionId = proxy_sesionId;
    }

    public String getOs_userName() {
        return os_userName;
    }

    public void setOs_userName(String os_userName) {
        this.os_userName = os_userName;
    }

    public String getUserHost() {
        return userHost;
    }

    public void setUserHost(String userHost) {
        this.userHost = userHost;
    }

    public String getActian_name() {
        return actian_name;
    }

    public void setActian_name(String actian_name) {
        this.actian_name = actian_name;
    }

    public String getDbUserName() {
        return dbUserName;
    }

    public void setDbUserName(String dbUserName) {
        this.dbUserName = dbUserName;
    }

    public String getSql_Text() {
        return sql_Text;
    }

    public void setSql_Text(String sql_Text) {
        this.sql_Text = sql_Text;
    }

    public String getClient_program_name() {
        return client_program_name;
    }

    public void setClient_program_name(String client_program_name) {
        this.client_program_name = client_program_name;
    }

    public String getEvent_timestamp() {
        return event_timestamp;
    }

    public void setEvent_timestamp(String event_timestamp) {
        this.event_timestamp = event_timestamp;
    }

    public String getObject_name() {
        return object_name;
    }

    public void setObject_name(String object_name) {
        this.object_name = object_name;
    }

    public String getAuthentication_type() {
        return authentication_type;
    }

    public void setAuthentication_type(String authentication_type) {
        this.authentication_type = authentication_type;
    }

    public String getCurrentyDate() {
        return currentyDate;
    }

    public void setCurrentyDate(String currentyDate) {
        this.currentyDate = currentyDate;
    }
}
