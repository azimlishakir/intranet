package auditing.controller;

import auditing.dto.RoleDTO;
import auditing.dto.UsersDTO;
import auditing.repository.UsersRepository;
import auditing.service.UsersService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UsersController.class)
@ActiveProfiles("default")
@WithMockUser
public class UsersControllerTest {

    private static final String API = "/users";
    private static final String APIBYID = "/users/{id}";
    private static final String APIALL = "/users/all";


    private static final String ID = "$.id";
    private static final String NAME = "$.name";
    private static final String LASTNAME = "$.lastname";
    private static final String USERNAME = "$.username";
    private static final String EMAIL = "$.email";
    private static final String PASSWORD = "$.password";
    private static final String ROLE_NAME = "$.role.name";

    private static final String USR_NAME = "Shakir";
    private static final String USR_LASTNAME = "Azimli";
    private static final String USR_USERNAME = "shakir.azimli";
    private static final String USR_EMAIL = "azimlishakir@gmail.com";
    private static final String USR_PASSWORD = "123456";
    private static final long USER_ID = 1;
    private static final long ROLE_ID = 1;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    private UsersDTO usersDTO;
    private RoleDTO roleDTO;

    @MockBean
    private UsersService usersService;

    @MockBean
    private UsersRepository usersRepository;

    @BeforeEach
    public void setUp() {
        SecurityContext context = new SecurityContextImpl();
         roleDTO = RoleDTO.builder().
                id(ROLE_ID).
                name("ADMINISTRATOR").build();


        usersDTO = UsersDTO.builder().
                id(USER_ID).
                name(USR_NAME).
                lastname(USR_LASTNAME).
                username(USR_USERNAME).
                email(USR_EMAIL).
                password(USR_PASSWORD).
                role(roleDTO).
                build();

    }



    @Test
    public void allUsers() throws Exception {
        when(usersService.listUsers()).thenReturn(Collections.EMPTY_LIST);
        mvc.perform( get(APIALL)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    public void getUsersById() throws Exception{
        when(usersService.getUsers(USER_ID)).thenReturn(usersDTO);

        mvc.perform(get(APIBYID,USER_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(usersDTO.getId()));
    }


    @Test
    public void saveUsers() throws Exception{
        when(usersService.createUsers(usersDTO)).thenReturn(usersDTO);
        mvc.perform(post(API)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(usersDTO))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(ID).value(usersDTO.getId()))
                .andExpect(jsonPath(NAME).value(usersDTO.getName()))
                .andExpect(jsonPath(LASTNAME).value(usersDTO.getLastname()))
                .andExpect(jsonPath(USERNAME).value(usersDTO.getUsername()))
                .andExpect(jsonPath(EMAIL).value(usersDTO.getEmail()))
                .andExpect(jsonPath(PASSWORD).value(usersDTO.getPassword()))
                .andExpect(jsonPath(ROLE_NAME).value(roleDTO.getName()))
                .andDo(print());
    }

    @Test
    public void updateUsers() throws Exception{
        when(usersService.updateUsers(usersDTO,USER_ID)).thenReturn(usersDTO);
        mvc.perform(put(APIBYID,ROLE_ID)
                .content(objectMapper.writeValueAsString(usersDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(usersDTO.getId()))
                .andExpect(jsonPath(NAME).value(usersDTO.getName()))
                .andExpect(jsonPath(LASTNAME).value(usersDTO.getLastname()))
                .andExpect(jsonPath(USERNAME).value(usersDTO.getUsername()))
                .andExpect(jsonPath(EMAIL).value(usersDTO.getEmail()))
                .andExpect(jsonPath(PASSWORD).value(usersDTO.getPassword()))
                .andExpect(jsonPath(ROLE_NAME).value(roleDTO.getName()))
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    public void deleteUsers() throws Exception{
      //  when(usersService.deleteUsers(USER_ID)).thenReturn(usersDTO);
        mvc.perform(delete(APIBYID,USER_ID)
                .content(objectMapper.writeValueAsString(usersDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(ID).value(usersDTO.getId()));
    }

}