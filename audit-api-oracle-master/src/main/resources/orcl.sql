select a.ROWNUM,
       a.AUDIT_TYPE,
       a.SESSIONID,
       a.PROXY_SESSIONID,
       a.OS_USERNAME,
       a.USERHOST,
       a.DBUSERNAME,
       a.SQL_TEXT,
       a.CLIENT_PROGRAM_NAME,
       a.EVENT_TIMESTAMP,
       a.ACTION_NAME,
       a.OBJECT_NAME,
       a.AUTHENTICATION_TYPE
from unified_audit_trail a
where a.object_schema in ('GAIA', 'ULOG', 'PLFLOW')
order by EVENT_TIMESTAMP desc;


select a.audit_type,
       a.userhost,
       a.client_program_name,
       a.os_username,
       a.dbusername,
       a.event_timestamp,
       a.object_schema,
       a.object_name,
       a.sql_text,
       a.sql_binds
from unified_audit_trail a
where a.object_schema in ('GAIA', 'ULOG', 'PLFLOW')
order by 6 desc;