package auditingoracle.auditapi.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AudittrailMapper implements RowMapper<AudTrialDto> {


    @Override
    public AudTrialDto mapRow(ResultSet res, int i) throws SQLException {
        AudTrialDto audTrialDto = new AudTrialDto();
        audTrialDto.setAuditType(res.getString("AUDIT_TYPE"));
        audTrialDto.setSesionId(res.getString("SESSIONID"));
        audTrialDto.setProxy_sesionId(res.getString("PROXY_SESSIONID"));
        audTrialDto.setOs_userName(res.getString("OS_USERNAME"));
        audTrialDto.setUserHost(res.getString("USERHOST"));
        audTrialDto.setDbUserName(res.getString("DBUSERNAME"));
        audTrialDto.setSql_Text(res.getString("SQL_TEXT"));

        audTrialDto.setClient_program_name(res.getString("CLIENT_PROGRAM_NAME"));
        audTrialDto.setEvent_timestamp(res.getString("EVENT_TIMESTAMP"));
        audTrialDto.setActian_name(res.getString("ACTION_NAME"));
        audTrialDto.setObject_name(res.getString("OBJECT_NAME"));
        audTrialDto.setAuthentication_type(res.getString("AUTHENTICATION_TYPE"));
        audTrialDto.setCurrentyDate(res.getString("EVENT_TIMESTAMP"));

        return audTrialDto;
    }
}
