<?php
@session_start();
require_once "./db/db.php";
require_once "./functions/function.php";
require_once "./db/config.php";

$email=@$_SESSION["email"];
$pass=@$_SESSION["pass"];

$usrid=$db->prepare("SELECT * FROM users WHERE email=?");
$usrid->execute(array($email));
$fetchid=$usrid->fetch(PDO::FETCH_ASSOC);
$userid=$fetchid["id"];
$dep_id = $fetchid['dep_id'];
$sub_deps = $db->query("select * from sub_departaments where dep_id='{$dep_id}'")->fetchAll(PDO::FETCH_ASSOC);

if (!empty($email)) {

    if (isset($_POST['gonder'])) {
        $usrtitle = $_POST['title'];
        $usrtarix = $_POST['date_insert'];
        $usrmetin = $_POST['post'];
        $sub_dep  = $_POST['sub_dep_id'];


        $sekil = @$_FILES['img']['name'];
        $sekiltmp=@$_FILES['img']['tmp_name'];
        $sekilurl="views/post/img/".$sekil;
        $sekilyukle=@move_uploaded_file($sekiltmp,$sekilurl);


        $pdf=$_FILES['pdf']['name'];
        $pdftmp = $_FILES['pdf']['tmp_name'];
        $pdfurl = "views/post/pdf/".$pdf;
        $unique1=rand(100,999);
        $unique2=rand(1000,9999);
        $unique3=rand(10000,99999);
        $unique=$unique1.$unique2.$unique3;
        $type=explode('.', $pdf);
        $type=end($type);
        if ($pdf <=600000 && in_array($pdf, ['pdf']) &&
            $pdfyukle = move_uploaded_file($pdftmp, 'views/post/pdf/'.$unique.'.'.$type)){

        }



        $word = @$_FILES['word']['name'];
        $wordtmp=@$_FILES['word']['tmp_name'];
        $wordurl="views/post/word/".$word;
        $wordyukle= @move_uploaded_file($wordtmp,$wordurl);

        $excel = @$_FILES['excel']['name'];
        $exceltmp = @$_FILES['excel']['tmp_name'];
        $excelurl = "views/post/excel".$excel;
        $excelyukle = @move_uploaded_file($exceltmp,$excelurl);


        if (empty($usrtarix)){
            date_default_timezone_set("Asia/Baku");
            $indikitarix=date("Y-m-d");
            $post = $db->prepare("INSERT user_post SET user_id=?,post=?,title=?,date_insert=?,row_status=?,sub_dep=?,image=?,pdf=?,word=?,excel=?");
            $usrelave = $post->execute([$userid, $usrmetin, $usrtitle, $indikitarix,1,$sub_dep,$sekilurl,$unique.'.'.$type ,$wordurl,$excelurl]);

            if ($usrelave) {
                echo "<script>alert('Melumat elave edildi')</script>";
                header("refresh:1 url=".SITE_URL."/?folder=post&page=all");
            }

        }
        else {

            $post = $db->prepare("INSERT user_post SET user_id=?,post=?,title=?,date_insert=?,row_status=?,sub_dep=?,image=?,pdf=?,word,excel");
            $usrelave = $post->execute(array($userid, $usrmetin, $usrtitle, $usrtarix, 1 ,$sub_dep,$sekilurl,$unique.'.'.$type ,$wordurl,$excelurl));

            if ($usrelave) {
                echo "<script>alert('Melumat elave edildi')</script>";
                header("refresh:20;url=".SITE_URL."/?folder=post&page=all");
            }
        }

    }


    echo "
<div class='pull-left all_posts col-md-12'>

    <form class='form-horizontal' role='form' action='' method='post' autocomplete='off' enctype='multipart/form-data'>

       <div class='col-md-8 pull-left container'>



            ";




    echo "

                   <div class='form-group'>
                   <label> Başlıq </label>
                       <input type='text' name= 'title' autocomplete='off' class='form-control' />
                   </div>
                   
                   <div class='form-group'>
                   
                          <label class='my-1 mr-2' for='inlineFormCustomSelectPref'>Bölmələr</label>
                            <select name='sub_dep_id' class='custom-select my-1 mr-sm-2' id='inlineFormCustomSelectPref'>
                             <option selected>Seçin...</option>
                             ";
    foreach ($sub_deps as $sub_dep){
        echo "<option value='$sub_dep[id]'>$sub_dep[name]</option>";
    }
    echo"
                           </select>
                             </div>
                   
                           
                   <div class='form-group'>
                   <label> Tarix </label>
                       <input type='date' name= 'date_insert' autocomplete='off' class='form-control' />
                   </div>
                 
                       
                   <div class='form-group custom-file'>
                        <label class='' for='img'>Şəkil</label> <br />
                        <input type='file' name='img' class='sekil' accept='image/*'>
                    </div><br><br>


                      <div class='form-group custom-file'>
                        <label class='' for='pdf'>PDF file</label> <br />
                        <input type='file' name='pdf' class='sekil' accept='application/pdf'>
                    </div><br><br>

                    <div class='form-group custom-file'>
                        <label class='' for='word'>World file</label> <br />
                        <input type='file' name='word' class='sekil' accept='application/msword'>
                    </div><br><br>
                    
                     <div class='form-group custom-file'>
                        <label class='' for='excel'>World file</label> <br />
                        <input type='file' name='excel' class='fa-file-excel' accept='application/vnd.ms-excel'>
                    </div><br><br>


                   
                       <div class='form-group'>
                   <label> Mətn </label>
                        <textarea placeholder='Mətin' name='post' autocomplete='off' class='ckeditor' id='ckeditor' style='resize:none'></textarea>
                        </div>
                        
                        <div class='form-group'>
                      
            
                    <button type='submit' name='gonder' class='btn btn-success full_width'><i class='glyphicon glyphicon-plus'></i>Gonder</button>

                </div>
                
                     
                   
                   

  
            ";


    echo "<div class='tab-content'>";


    echo "

               <div class='form-group tab-pane fade' id='tab_'>

                   <textarea class='form-control editor' id='editor_' name=''></textarea>
               </div>";


    echo '</div>';


    echo "

           
              </div>


            


    </form>


</div>



";
}


?>
