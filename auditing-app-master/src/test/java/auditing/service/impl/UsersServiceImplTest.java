package auditing.service.impl;
import auditing.dto.RoleDTO;
import auditing.dto.UsersDTO;
import auditing.exception.RoleNotFoundException;
import auditing.exception.UserNotFoundException;
import auditing.exception.UsersNotFoundIdException;
import auditing.model.Role;
import auditing.model.Users;
import auditing.repository.RoleRepository;
import auditing.repository.UsersRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@ExtendWith(SpringExtension.class)
@WithMockUser
@SpringBootTest
@RunWith(SpringRunner.class)
public class UsersServiceImplTest {

    private static final String USR_NAME = "Shakir";
    private static final String USR_LASTNAME = "Azimli";
    private static final String USR_USERNAME = "shakir.azimli";
    private static final String USR_EMAIL = "azimlishakir@gmail.com";
    private static final String USR_PASSWORD = "123456";
    private static final long USER_ID = 1;
    private static final long ROLE_ID = 1;

    @Mock
    private UsersRepository usersRepository;


    @Mock
    private RoleRepository roleRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UsersServiceImpl usersService;

    private UsersDTO usersDTO;

    private RoleDTO roleDTO;

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();


    @BeforeEach
    public void setUp() {
        System.out.println("init");
        roleDTO = RoleDTO.builder().
                id(ROLE_ID).
                name("ADMINISTRATOR").build();


        usersDTO = UsersDTO.builder().
                id(USER_ID).
                name(USR_NAME).
                lastname(USR_LASTNAME).
                username(USR_USERNAME).
                email(USR_EMAIL).
                password(USR_PASSWORD).
                role(roleDTO).
                build();

    }

    @Test
    public void listUserServiceTest() throws Exception{
        Role role = new Role(1L,"ADMINISTRATOR");
        Users users = new Users(USER_ID,USR_NAME,USR_LASTNAME,USR_USERNAME,USR_EMAIL,USR_PASSWORD,role);
        when(usersRepository.findAll()).thenReturn(Arrays.asList(users));
        List<Users> list = usersService.listUsers();
        assertEquals(1,list.size());
        verify(usersRepository).findAll();
    }

    @Test
    public void GetUserByIdSuccess(){
        Role role = new Role(1L,"ADMINISTRATOR");
        Users users = new Users(USER_ID,USR_NAME,USR_LASTNAME,USR_USERNAME,USR_EMAIL,USR_PASSWORD,role);
        when(usersRepository.findById(USER_ID)).thenReturn(Optional.of(users));
        UsersDTO list = usersService.getUsers(1L);
        assertEquals(users.getId(),list.getId());
        assertEquals(users.getEmail(),list.getEmail());
        assertEquals(users.getLastname(),list.getLastname());
        assertEquals(users.getEmail(),list.getEmail());
        assertEquals(users.getPassword(),list.getPassword());
        assertNotEquals(users,usersDTO);
        assertNotNull(users);
        assertNotNull(list);
        verify(usersRepository).findById(USER_ID);
    }

    @Test
    public void GetUserByIdNotFound(){
        when(usersRepository.findById(USER_ID)).thenReturn(Optional.empty());
        exceptionRule.expect(UserNotFoundException.class);
        exceptionRule.expectMessage("User not found");
        usersService.getUsers(1L);
        verify(usersRepository).findById(USER_ID);
    }

    @Test
    public void createUsersTestSuccess(){
        Role role = new Role(1L,"ADMINISTRATOR");
        Users users = new Users(USER_ID,USR_NAME,USR_LASTNAME,USR_USERNAME,USR_EMAIL,USR_PASSWORD,role);
        UsersDTO expected = UsersDTO.builder().
                email(USR_EMAIL).
                password(USR_PASSWORD).
                username(USR_USERNAME).
                role(RoleDTO.builder().id(1L).name("ADMINISTRATOR").build()).
                lastname(USR_LASTNAME).
                name(USR_NAME).build();


        when(roleRepository.findById(expected.getRole().getId())).thenReturn(Optional.of(role));
        when(passwordEncoder.encode(expected.getPassword())).thenReturn(USR_PASSWORD);
        when(usersRepository.save(any())).thenReturn(users);
        UsersDTO actual = usersService.createUsers(expected);
        assertEquals(expected.getLastname(),actual.getLastname());
        assertEquals(expected.getEmail(),actual.getEmail());
        assertEquals(expected.getUsername(),actual.getUsername());
        assertEquals(expected.getPassword(),actual.getPassword());

    }


    @Test
    public void createUsersTestError(){
        UsersDTO dto = UsersDTO.builder().
                id(USER_ID).
                email(USR_EMAIL).
                password(USR_PASSWORD).
                username(USR_USERNAME).
                role(RoleDTO.builder().id(1L).name("ADMINISTRATOR").build()).
                lastname(USR_LASTNAME).
                name(USR_NAME).build();

        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("Users id must be null");
        usersService.createUsers(dto);
    }

    @Test
    public void updateUsersSuccess(){
        Role role = new Role(1L,"ADMINISTRATOR");
        Users users = new Users(USER_ID,USR_NAME,USR_LASTNAME,USR_USERNAME,USR_EMAIL,USR_PASSWORD,role);
        UsersDTO expected = UsersDTO.builder().
                email(USR_EMAIL).
                password(USR_PASSWORD).
                username(USR_USERNAME).
                role(RoleDTO.builder().id(1L).name("ADMINISTRATOR").build()).
                lastname(USR_LASTNAME).
                name(USR_NAME).build();

        when(roleRepository.findById(expected.getRole().getId())).thenReturn(Optional.of(role));
        when(passwordEncoder.encode(expected.getPassword())).thenReturn(USR_PASSWORD);
        when(usersRepository.save(any())).thenReturn(users);
        UsersDTO actual = usersService.updateUsers(expected,USER_ID);
        assertEquals(expected.getId(),actual.getId());

    }

    @Test
    public void deleteUsersTest(){
        final Role role = new Role(1L,"ADMINISTRATOR");
        Users users = Users.builder()
                .id(USER_ID)
                .name(USR_NAME)
                .lastname(USR_LASTNAME)
                .username(USR_USERNAME)
                .email(USR_EMAIL)
                .password(USR_PASSWORD)
                .role(role)
                .build();
        when(usersRepository.findById(USER_ID)).thenReturn(Optional.of(users));
        usersService.deleteUsers(USER_ID);
        verify(usersRepository).deleteById(users.getId());
        verify(usersRepository).findById(users.getId());
    }

    @Test
    public void deleteUsersIdTest() throws IllegalArgumentException {
        final Role role = new Role(1L,"ADMINISTRATOR");
        Users users = Users.builder()
                .id(USER_ID)
                .name(USR_NAME)
                .lastname(USR_LASTNAME)
                .username(USR_USERNAME)
                .email(USR_EMAIL)
                .password(USR_PASSWORD)
                .role(role)
                .build();
        when(usersRepository.findById(USER_ID)).thenReturn(Optional.empty());
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("No users with id");
        usersService.deleteUsers(users.getId());
    }


    @Test
    public void loadUserByUsername(){
        Role role = new Role(1L,"ADMINISTRATOR");
        Users users = new Users(USER_ID,USR_NAME,USR_LASTNAME,USR_USERNAME,USR_EMAIL,USR_PASSWORD,role);
        when(usersRepository.findByUsername(USR_USERNAME)).thenReturn(Optional.of(users));
        assertEquals(users,usersService.loadUserByUsername(USR_USERNAME));
    }

    @Test
    public void loadUserByUsernameException(){
        Role role = new Role(1L,"ADMINISTRATOR");
        Users users = new Users(USER_ID,USR_NAME,USR_LASTNAME,USR_USERNAME,USR_EMAIL,USR_PASSWORD,role);
        when(usersRepository.findByUsername(USR_USERNAME)).thenReturn(Optional.empty());
        exceptionRule.expect(UsernameNotFoundException.class);
        exceptionRule.expectMessage("Invalid username");
        assertEquals(users,usersService.loadUserByUsername(USR_USERNAME));

    }


    @Test
    public void findRoleWithNameSuccessTest(){
        Role role = new Role(1L,"ADMINISTRATOR");
        when(roleRepository.findByName("ADMINISTRATOR")).thenReturn(Optional.of(role));
        assertEquals(role,usersService.findRoleWithName("ADMINISTRATOR"));
    }

    @Test
    public void findRoleWithNameExceptionTest(){
        Role role = new Role(1L,"ADMINISTRATOR");
        when(roleRepository.findByName("ADMINISTRATOR")).thenReturn(Optional.empty());
        exceptionRule.expect(RoleNotFoundException.class);
        exceptionRule.expectMessage("Role not found with name");
        assertEquals(role,usersService.findRoleWithName("ADMINISTRATOR"));
    }



}