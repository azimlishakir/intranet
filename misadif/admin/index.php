<?php
require_once "db/config.php";
require_once "db/db.php";
require_once "functions/function.php";
require_once "functions/admin_function.php";

require_once "./views/template/header.php";

if (isset($_GET['folder']) && $_GET['page']){
    $folder=saniteUrl($_GET['folder']);
    $page= saniteUrl($_GET['page']);
    $file = './views/' .$folder. '/' .$page. '.php';
    if (file_exists($file)){
        require_once $file;
    }else{
        header('location: ' .SITE_URL);exit;
    }

}else{
    require_once "./views/home/index.php";
}

require_once "./views/template/footer.php";



?>