package auditing.exception;

public class RoleNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public RoleNotFoundException(String name) {
        super(String.format("Role not found with name %n",name));
    }
}
