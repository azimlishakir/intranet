#!/bin/bash

FILE=Dockerfile
PORT=8085
#IMAGE_NAME=$(date +"%Y_%m_%d")
TAG_NAME=$(date +"%H_%M")

#Docker Register
URL=gitlab.adif.az:5005/
groups=$1
repo=$2


function writeData(){
    echo "FROM openjdk:8-jdk-alpine
ADD target/*.jar *.jar
EXPOSE $PORT
ENTRYPOINT [\"java\", \"-jar\", \"*.jar\"]
    " > $FILE
}

function dockerPushRepo(){
    docker build -t $URL$groups/$repo:$TAG_NAME .
    docker push $URL$groups/$repo:$TAG_NAME
}


if [[ -z $1 ]]
then
    echo "group yoxdur"
    exit
elif [[ -z $2 ]]
then
    echo "repo yoxdur"
    exit
elif [[ -v $FILE ]]
then
    echo  "Bele file  var"
    dockerPushRepo
else
    touch $FILE
    echo "$FILE yaradildi"
    writeData
    dockerPushRepo 
fi