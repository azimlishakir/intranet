package auditingoracle.auditapi.controller;


import auditingoracle.auditapi.model.AudTrialDto;
import auditingoracle.auditapi.services.AudServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path ="/aud-api")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AudController {

    @Autowired
    private AudServices audServices;

    @GetMapping(path = "/test")
    public String test(){
        return "test z";
    }

    @GetMapping(path = "/all-data")
    public ResponseEntity<List<AudTrialDto>> getAllData(){
        return ResponseEntity.ok(audServices.getAudDate());
    }

//    @RequestMapping(value = "action_name/{action_name}",method = RequestMethod.GET)
////    @ResponseBody
////    public ResponseEntity<List<AudTrialDto>> findByActionName(@PathVariable String action_name){
////        System.out.println("sss--"+action_name);
////        return ResponseEntity.ok(audServices.getFindByActionName(action_name));
////    }


    @PostMapping("action_name/audTrialDto")
    public ResponseEntity<List<AudTrialDto>> findBy(@RequestBody AudTrialDto audTrialDto){
        System.out.println("sss--"+audTrialDto.getActian_name());
        System.out.println("tarix  =="+audTrialDto.getEvent_timestamp());
        return ResponseEntity.ok(audServices.getFindByActionName(audTrialDto.getActian_name(),
                audTrialDto.getEvent_timestamp(),audTrialDto.getCurrentyDate(),audTrialDto.getOs_userName()
                ,audTrialDto.getObject_name() ,audTrialDto.getUserHost(),audTrialDto.getDbUserName()));
    }

    @PostMapping("action_name/audTrialDto_")
    public ResponseEntity<List<AudTrialDto>> findBy_(@RequestBody AudTrialDto audTrialDto){
        System.out.println("sss--"+audTrialDto.getActian_name());
        System.out.println("tarix  =="+audTrialDto.getEvent_timestamp());
        return ResponseEntity.ok(audServices.getFindByActionName_(audTrialDto));
    }



}
