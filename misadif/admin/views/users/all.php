<?php
@session_start();
require_once "../db/db.php";
require_once "./db/config.php";
$email=@$_SESSION["email"];
$pass=@$_SESSION["password"];
$admins=$db->prepare("SELECT * FROM users WHERE vezife=0");
$admins->execute();
$adminrows=$admins->rowCount();

if(!empty($email)) {
    echo "

<div class='pull-left col-md-12 admin_top' style='margin-top: 30px'>
    <div class='col-md-12 pull-left'>
        <button class='btn btn-primary pull-left' type='button'>
                İstifadəçilər <span class='badge badge-light'>$adminrows</span>
        </button>
 <a href='".SITE_URL."/?folder=users&page=add' class='btn pull-right btn-success add'><i class='fas fa-plus'></i> Əlavə et</i></a>

    </div>


";

    echo "
        <table class='table table-bordered table-responsive table-hover table-striped' style='margin-top: 30px'>
            <thead>
                <tr>
                    <th>ID</th>
                    <th width='150'>AD</th>
                    <th width='200'>Soyad</th>
                    <th width='233'>Email</th>
                    <th width='200'>Mobile</th>
                    <th width='200'>Departament</th>
                    <th width='300'>Əməliyyat</th>             
                </tr>
            </thead>

";
    $sec = $db->prepare("SELECT * FROM users WHERE vezife=0");
    $sec->execute();
    $res = $sec->fetchAll(PDO::FETCH_ASSOC);
    foreach ($res as $okey) {
        $adminid = $okey["id"];
        $adminname = $okey["name"];
        $adminlastname = $okey["lastname"];
        $adminemail = $okey["email"];
        $adminmobile = $okey["mobile"];
        $depbolmeid=$okey["dep_id"];

        $departcek=$db->prepare("SELECT * FROM departament WHERE id=?");
        $departcek->execute(array($depbolmeid));
        $depbolmefetch=$departcek->fetch(PDO::FETCH_ASSOC);
        $deptitle=$depbolmefetch["title"];

        echo "
            <tbody>
                <tr>
                
                    <td>$adminid</td>
                    <td>$adminname</td>
                    <td>$adminlastname</td>
                    <td>$adminemail</td>
                    <td>$adminmobile</td>
                    <td>$deptitle</td>

                    <td>
                       
                        <a href='".SITE_URL."/?folder=users&page=edit&id=$adminid' class='btn btn-success'><i class='fas fa-edit'></i> Yenile </a>
                        <a href='".SITE_URL."/?folder=users&page=question_admin&id=$adminid' class='btn btn-danger'><i class='fas fa-trash-alt'></i> Sil </a>
                    </td>
                   
                </tr>
            </tbody>
            ";
    }
    echo "
            
        </table>


</div>
    
    ";
}
else{
    header("location:".SITE_URL."/login.php");
}
?>