package auditingoracle.auditapi.services;

import auditingoracle.auditapi.model.AudTrialDto;
import auditingoracle.auditapi.model.AudittrailMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class AudServices {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    public List<AudTrialDto> getAudDate(){
        List<AudTrialDto> list= new ArrayList<>();
//
        return jdbcTemplate.
                query("select *\n" +
                                "  from \n" +
                                "(select row_number () over ( order by u.EVENT_TIMESTAMP desc ) as rn , \n" +
                                " u.* from UNIFIED_AUDIT_TRAIL u  where\n" +
                                "  u.object_schema in ('GAIA', 'ULOG', 'PLFLOW'))  r \n" +
                                "where r.rn<= 100",
                        (resultSet, i) -> {
                            return toAudTrial(resultSet);
                        });
    }



    public List<AudTrialDto> getFindByActionName(String action_name,String  startDate,String  endDate,String os_username,String objectName
    ,String userhost,String dbuser){

        List<AudTrialDto> list= new ArrayList<>();
        System.out.println("testt");
        System.out.println("action Name = "+action_name);
        System.out.println("startDate = "+startDate);
        System.out.println("endDate = "+endDate);

        String date1 = startDate.replace("T"," ");
        String date2 = endDate.replace("T"," ");
        System.out.println("date1="+date1+" "+ " Date2 ="+date2);
        String sql = "select * from   (select row_number () over ( order by u.EVENT_TIMESTAMP desc ) as rn , u.* from UNIFIED_AUDIT_TRAIL u  where   u.object_schema in ('GAIA', 'ULOG', 'PLFLOW')) a where a.rn<= 100  and cast(a.EVENT_TIMESTAMP as date)>= cast(TO_DATE('"+date1+"','yyyy/mm/dd hh24:mi') as  date)  and cast(a.EVENT_TIMESTAMP as date)<= cast(TO_DATE('"+date2+"','yyyy/mm/dd hh24:mi') as  date)  and UPPER(a.action_name)= nvl(UPPER('"+action_name+"'),UPPER(a.action_name)) and UPPER(a.OS_USERNAME)= nvl(UPPER('"+os_username+"'),UPPER(a.OS_USERNAME)) and UPPER(a.object_name)= nvl(UPPER('"+objectName+"'), UPPER(a.object_name))   and upper(a.USERHOST)   = nvl(UPPER('"+userhost+"'),   upper(a.USERHOST))   and upper(a.DBUSERNAME) = nvl(UPPER('"+dbuser+"'),     upper(a.DBUSERNAME))";
        System.out.println("sql= "+sql);

        return jdbcTemplate.
                query(sql,

                        (resultSet, i) -> {
                            return toAudTrial(resultSet);
                        });
    }

    private AudTrialDto toAudTrial(ResultSet res) throws SQLException {
        AudTrialDto audTrialDto = new AudTrialDto();
        System.out.println("---------");
        System.out.println("res = "+res);
        System.out.println("res = "+res.getString(("AUDIT_TYPE")));
        audTrialDto.setAuditType(res.getString("AUDIT_TYPE"));
        audTrialDto.setSesionId(res.getString("SESSIONID"));
        audTrialDto.setProxy_sesionId(res.getString("PROXY_SESSIONID"));
        audTrialDto.setOs_userName(res.getString("OS_USERNAME"));
        audTrialDto.setUserHost(res.getString("USERHOST"));
        audTrialDto.setDbUserName(res.getString("DBUSERNAME"));
        audTrialDto.setSql_Text(res.getString("SQL_TEXT"));

        audTrialDto.setClient_program_name(res.getString("CLIENT_PROGRAM_NAME"));
        audTrialDto.setEvent_timestamp(res.getString("EVENT_TIMESTAMP"));
        audTrialDto.setActian_name(res.getString("ACTION_NAME"));
        audTrialDto.setObject_name(res.getString("OBJECT_NAME"));
        audTrialDto.setAuthentication_type(res.getString("AUTHENTICATION_TYPE"));
        audTrialDto.setCurrentyDate(res.getString("EVENT_TIMESTAMP"));


        return audTrialDto;
    }

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public List<AudTrialDto> getFindByActionName_(AudTrialDto audTrialDto){

        List<AudTrialDto> list= new ArrayList<>();
        System.out.println("testt");
        System.out.println("action Name = "+audTrialDto.getActian_name());
        System.out.println("startDate = "+audTrialDto.getEvent_timestamp());

        System.out.println("endDate = "+audTrialDto.getCurrentyDate());

        String date1 = audTrialDto.getEvent_timestamp().replace("T"," ");
        String date2 = audTrialDto.getCurrentyDate().replace("T"," ");
        System.out.println("date1="+date1+" "+ " Date2 ="+date2);
        String sql = "select * from   (select row_number () over ( order by u.EVENT_TIMESTAMP desc ) as rn , u.* from UNIFIED_AUDIT_TRAIL u  where   u.object_schema in ('GAIA', 'ULOG', 'PLFLOW')) a where a.rn<= 100  and cast(a.EVENT_TIMESTAMP as date)>= cast(TO_DATE('"+date1+"','yyyy/mm/dd hh24:mi') as  date)  and cast(a.EVENT_TIMESTAMP as date)<= cast(TO_DATE('"+date2+"','yyyy/mm/dd hh24:mi') as  date)  and UPPER(a.action_name)= nvl(UPPER( :action_name),UPPER(a.action_name)) and UPPER(a.OS_USERNAME)= nvl(UPPER( :os_username),UPPER(a.OS_USERNAME)) and UPPER(a.object_name)= nvl(UPPER( :objectName), UPPER(a.object_name))   and upper(a.USERHOST)   = nvl(UPPER( :userhost),   upper(a.USERHOST))   and upper(a.DBUSERNAME) = nvl(UPPER( :dbuser),     upper(a.DBUSERNAME))";
        System.out.println("sql ww= "+sql);

        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(audTrialDto);




        return  jdbcTemplate.query("select r.rn,r.AUDIT_TYPE,r.SESSIONID,r.OS_USERNAME,r.PROXY_SESSIONID,r.USERHOST,r.DBUSERNAME,r.SQL_TEXT,r.CLIENT_PROGRAM_NAME,r.EVENT_TIMESTAMP,r.ACTION_NAME,r.OBJECT_NAME,r.AUTHENTICATION_TYPE from (select row_number () over ( order by u.EVENT_TIMESTAMP desc ) as rn , u.* from UNIFIED_AUDIT_TRAIL u  where u.object_schema in ('GAIA', 'ULOG', 'PLFLOW'))  r where  r.ACTION_NAME = nvl(upper( ? ),r.ACTION_NAME)  and r.OBJECT_NAME = nvl(upper( ? ),r.OBJECT_NAME) and upper(r.OS_USERNAME) = nvl(upper( ? ) ,upper(r.OS_USERNAME))  and  r.USERHOST = nvl(upper( ? ),r.USERHOST)  and  r.DBUSERNAME = nvl(upper( ? ),r.DBUSERNAME) and  r.CLIENT_PROGRAM_NAME = nvl(upper( ? ),r.CLIENT_PROGRAM_NAME) and trunc(r.EVENT_TIMESTAMP) >= trunc(to_date( ? , 'YYYY-MM-DD HH24:MI'))  and trunc(r.EVENT_TIMESTAMP) <= trunc(to_date( ? , 'YYYY-MM-DD HH24:MI'))" ,
                new Object[] { audTrialDto.getActian_name(),  audTrialDto.getObject_name(),audTrialDto.getOs_userName(),audTrialDto.getUserHost(),audTrialDto.getDbUserName(),audTrialDto.getClient_program_name(),date2,date1 },
                new AudittrailMapper());

    }






}
