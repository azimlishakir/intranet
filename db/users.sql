-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 09, 2019 at 12:55 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `adif`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `mobile` varchar(40) NOT NULL,
  `user_type` int(11) NOT NULL,
  `dep_id` int(11) NOT NULL,
  `vezife` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `lastname`, `email`, `password`, `mobile`, `user_type`, `dep_id`, `vezife`) VALUES
(1, 'Shakir', 'Azimli', 'azimlishakir@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '+994504102050', 0, 1, 0),
(8, 'Elmar', 'Azimli', 'megnum_690@mail.ru', 'e10adc3949ba59abbe56e057f20f883e', '+994507666660', 0, 8, 0),
(9, 'Eldar', 'Memmedov', 'eldar@adif.az', 'e10adc3949ba59abbe56e057f20f883e', '+994502256585', 0, 7, 0),
(10, 'Musa', 'Salimov', 'musa@adif.az', 'e10adc3949ba59abbe56e057f20f883e', '+994502219656', 0, 0, 1),
(11, 'orxan', 'R', 'orkhan.rasulov@adif.az', 'e10adc3949ba59abbe56e057f20f883e', '0552066176', 0, 0, 1),
(12, 'Rufet', 'Rufet', 'rufat@adif.az', 'e10adc3949ba59abbe56e057f20f883e', '+994502265698', 0, 9, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
