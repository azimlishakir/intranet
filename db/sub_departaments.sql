-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 09, 2019 at 12:55 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `adif`
--

-- --------------------------------------------------------

--
-- Table structure for table `sub_departaments`
--

CREATE TABLE `sub_departaments` (
  `id` int(11) NOT NULL,
  `dep_id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `link` varchar(55) NOT NULL,
  `namespace` varchar(25) NOT NULL,
  `kod` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_departaments`
--

INSERT INTO `sub_departaments` (`id`, `dep_id`, `name`, `link`, `namespace`, `kod`) VALUES
(2, 1, 'Kuratorlar', 'kuratorlar', 'curators-container', 'curator'),
(3, 1, 'Xəbərlər', 'xeberler', 'news-container', 'news'),
(4, 7, 'Büdcə qanunvericiliyi', '', 'budce-container', 'budce'),
(5, 7, 'Hesabat', '', 'hesabat-container', 'hesabat'),
(6, 8, 'Xəbərlər', 'xeberler', 'xeber-container', 'law_news'),
(7, 9, 'Xəbərlər', 'xeberler', 'xeber1-container', 'ipr_news'),
(10, 9, 'Hesabatlar', 'hesabat', 'hes-container', 'ipr_hes'),
(11, 9, 'Mətbuat Xidmətləri', 'metbuat xidmetleri', 'metbuat-container', 'ipr_met'),
(12, 10, 'ESD haqqında təlimat', 'esd haqqinda telimat', 'esd-container', 'gd_about'),
(13, 10, 'Xəbərlər', 'xeberler', 'xeb3-container', 'gd_news'),
(14, 11, ' İnformasiya təhlükəsizliyi', 'Informasiya tehlukesizliyi', 'it-container', 'it_sec'),
(15, 11, ' HÖP-ə İnteqrasiya', 'HOP-e integrasiya', 'odenish-container', 'it_pay');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sub_departaments`
--
ALTER TABLE `sub_departaments`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sub_departaments`
--
ALTER TABLE `sub_departaments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
