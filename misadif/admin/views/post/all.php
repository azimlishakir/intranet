<?php
@session_start();
require_once "../db/db.php";
require_once "./db/config.php";
$email=@$_SESSION["email"];
//$uid = $_SESSION['id'];
$dep_id = @$_SESSION['dep_id'];
$pass=@$_SESSION["password"];
$post =$db->prepare("SELECT * FROM users");
$post->execute();
$p=$post->fetchAll(PDO::FETCH_ASSOC);
$post =$db->prepare("SELECT tb1.*,tb1.id as pid FROM user_post as tb1 LEFT JOIN users as tb2  ON tb1.user_id = tb2.id WHERE tb2.dep_id=?");
$post->execute([$dep_id]);
$p=$post->fetchAll(PDO::FETCH_ASSOC);
$postsay=$post->rowCount();

if (!empty($email)) {

    echo "

<div class='pull-left col-md-12 admin_top' style='margin-top: 30px'>
    <div class='col-md-12 pull-left'>
        <button class='btn btn-primary pull-left' type='button'>
                Postlar <span class='badge badge-light'>$postsay</span>
        </button>
 <a href='" . SITE_URL . "/?folder=post&page=add' class='btn pull-right btn-success add'><i class='fas fa-plus'></i> Əlavə et</i></a>

    </div>
    ";

    echo '
        <!--
    <div class="alert alert-warning alert-dismissible" role="alert" style="margin-top: 10px">
  <strong>Xəbərdarlıq!</strong> Əlavə olunmuş post tapılmadı!
</div>-->
    ';


    echo "




    <table class='table table-bordered table-responsive table-hover table-striped all_posts pull-left'  style='margin-top: 30px'>
        <thead>
                <tr>

                    <th width='200'> ID </th>
                    <th width='250'> Başlıq </th>
                    <th width='250'> Əlavə olunma tarixi </th>
                  
                <!--     <th width='200'> Status </th>-->
                    <th width='250'> Əməliyyat </th>

                </tr>

        </thead>
            <tbody>
            ";

            $post =$db->prepare("SELECT tb1.*,tb1.id as pid FROM user_post as tb1 LEFT JOIN users as tb2  ON tb1.user_id = tb2.id WHERE tb2.dep_id=?");
            $post->execute([$dep_id]);
            $p=$post->fetchAll(PDO::FETCH_ASSOC);
            $postsay=$post->rowCount();
            foreach ($p as $k) {
                $postid=$k["pid"];
                $postdesc=$k["post"];
                $posttitle=$k["title"];
                $postdate=$k["date_insert"];
                echo "

                <tr>
                    <td> $postid</td>
                    <td> ".$posttitle." </td>
                    <td> $postdate</td>
            

                    
                    ";


                echo "




                    <span class='label'>
                    <i class='glyphicon  '></i>
                    </span>

                    </td>
                    <td>

                        <a href='" . SITE_URL . "/index.php?folder=post&page=edit&id=$postid' class='btn btn-success'><i class='glyphicon glyphicon-pencil'></i> Yenilə</a>
                        <a href='" . SITE_URL . "/index.php?folder=post&page=question_post&id=$postid' class='btn btn-danger'><i class='glyphicon glyphicon-trash'></i> Sil</a>

                    </td>
                </tr>
                ";
            }

    echo "
            </tbody>

    </table>



    <nav>
        <ul class='pagination post_pagination'>
        ";


    echo "
                <!--
                <li>
                    <a href='" . SITE_URL . "/?folder=post&page=all&s=' aria-label='Previous'>
                        <span aria-hidden='true'>&laquo;</span>
                    </a>
                </li>-->
            ";


    echo "
                <!--<li class='active'><a href='" . SITE_URL . "/?folder=post&page=all&s='>1</a></li>-->
            ";


    echo "
                <!--<li>
                    <a href='" . SITE_URL . "/?folder=post&page=all&s=' aria-label='Next'>
                        <span aria-hidden='true'>&raquo;</span>
                    </a>
                </li>-->
            ";


    echo "
        </ul>
    </nav>
        ";


    echo "



</div>
    ";

}
?>