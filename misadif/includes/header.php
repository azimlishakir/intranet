﻿
<!doctype html>
<!--[if lt IE 7]>      <html class='no-js lt-ie9 lt-ie8 lt-ie7' lang=''> <![endif]-->
<!--[if IE 7]>         <html class='no-js lt-ie9 lt-ie8' lang=''> <![endif]-->
<!--[if IE 8]>         <html class='no-js lt-ie9' lang=''> <![endif]-->
<!--[if gt IE 8]><!-->
<html class='no-js' lang=''>
<!--<![endif]-->

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
    <title>ADIF MIS</title>
    <meta name='description' content=''>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='apple-touch-icon' href='apple-touch-icon.png'>


    <link rel='stylesheet' href='../public/assets/css/iconfont.css'>
    <link rel='stylesheet' href='../public/assets/fonts/stylesheet.css'>
    <link rel='stylesheet' href='../public/assets/css/font-awesome.min.css'>
    <link rel='stylesheet' href='../public/assets/css/jquery.fancybox.css'>
    <link rel='stylesheet' href='../public/assets/css/bootstrap.min.css'>
    <link rel='stylesheet' href='../public/assets/css/magnific-popup.css'>
    <link rel='stylesheet' href='../public/assets/css/jquery.dataTables.min.css'>
    <!--        <link rel='stylesheet' href='assets/css/bootstrap-theme.min.css'>-->


    <!--For Plugins external css-->
    <link rel='stylesheet' href='../public/assets/css/plugins.css' />

    <!--Theme custom css -->
    <link rel='stylesheet' href='../public/assets/css/style.css'>

    <!--Theme Responsive css-->
    <link rel='stylesheet' href='../public/assets/css/responsive.css' />

    <script src='../public/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js'></script>
    <style type='text/css'>
        .content-tab {
            display:none;
        }

        .content-tab.active {
            display:block;
        }
        .menu{
            position: relative;
           /* bottom: 35px;*/
            top: 15px;
            left: 0em;
        }

    </style>

</head>

<body data-spy='scroll' data-target='.navbar-collapse' style='font-family: Arial, Helvetica, sans-serif;'>
<!--[if lt IE 8]>
<p class='browserupgrade'>You are using an <strong>outdated</strong> browser. Please <a href='http://browsehappy.com/'>upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!--<div class='preloader'>
    <div class='loaded'>&nbsp;</div>
</div>-->
<div class='culmn'>
    <header id='main_menu' class='header navbar-fixed-top'>
        <div class='main_menu_bg'>
            <div class='container'>
                <div class='row'>
                    <div class='nave_menu'>
                        <nav class='navbar navbar-default'>
                            <div class='container-fluid'>
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class='navbar-header'>
                                    <button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#bs-example-navbar-collapse-1'
                                            aria-expanded='false'>
                                        <span class='sr-only'>Toggle navigation</span>
                                        <span class='icon-bar'></span>
                                        <span class='icon-bar'></span>
                                        <span class='icon-bar'></span>
                                    </button>
                                    <a class='navbar-brand' href='./index.php'>
                                        <img src='../public/assets/images/logo2.png' />
                                    </a>
                                </div>

                                <!-- Collect the nav links, forms, and other content for toggling -->



                                <div class='collapse navbar-collapse' id='bs-example-navbar-collapse-1'>

                                    <ul class='nav navbar-nav navbar-right menu'>
                                        <li>
                                            <a href='./index.php'><img src='../public/img/home.PNG'> Ana Səhİfə</a>
                                        </li>
                                        <li>
                                            <a href='http://172.23.9.15:8080/NumbersWeb/View/NumbersView.jsp' target="_blank"><img src='../public/img/users.svg' width="20px" height="20px"> Əməkdaşlar</a>
                                        </li>
                                        
                                        <li>
                                            <a href='http://mail.adif.az/' target='_blank'><img src='../public/img/mail.PNG'> E-maİl Gİrİş</a>
                                        </li>
                                     <!--   <li>
                                            <a href='http://helpdesk.adif.az/' target='_blank'><img src='../public/img/hd.PNG'>HELPDESK XİDMƏTİ</a>
                                        </li>-->
                                        <li>
                                            <a href='../elaqe.php'><img src='../public/img/elaqee.PNG'> Əlaqə</a>
                                        </li>

                                          

                                        <li>
                                            <a href='http://172.23.9.13/admin' target="_blank"><!--<img src='../public/img/elaqee.PNG'>-->GİRİŞ</a>
                                        </li>

                                    </ul>


                                </div>

                            </div>
                        </nav>
                    </div>
                </div>

            </div>

        </div>
    </header>
    <!--End of header -->
