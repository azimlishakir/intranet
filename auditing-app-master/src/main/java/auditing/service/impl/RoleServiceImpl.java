package auditing.service.impl;

import auditing.dto.RoleDTO;
import auditing.dto.UsersDTO;
import auditing.exception.RoleNotFoundException;
import auditing.exception.RoleNotFoundIDException;
import auditing.exception.UsersNotFoundIdException;
import auditing.mappers.RoleMapper;
import auditing.mappers.UserMapper;
import auditing.model.Role;
import auditing.repository.RoleRepository;
import auditing.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    private RoleMapper roleMapper;

    @Override
    public List listRole() {
        Iterable<Role> allRole = roleRepository.findAll();
        return Arrays.asList(allRole);
    }

    @Override
    public RoleDTO getRole(Long id) throws RoleNotFoundException{
        Role byId = roleRepository.findById(id)
                .orElseThrow(() -> new  RoleNotFoundException("Role not found with name"));
            return RoleMapper.INSTANCE.roleToDTO(byId);
    }


    @Override
    public RoleDTO createRole(RoleDTO roleDTO) throws RoleNotFoundIDException{
        if (roleDTO.getId() != null){
            throw  new RoleNotFoundIDException("Role id must be null");
        }
        Role role = RoleMapper.INSTANCE.role(roleDTO);
        System.out.println("role"+role.getName());
        return RoleMapper.INSTANCE.roleToDTO(roleRepository.save(role));
    }

    @Override
    public RoleDTO updateRole(RoleDTO roleDTO, Long id) {
        roleDTO.setId(id);
        Role role = RoleMapper.INSTANCE.role(roleDTO);
        return RoleMapper.INSTANCE.roleToDTO(roleRepository.save(role));
    }

    @Override
    public RoleDTO deleteRole(Long id) throws RoleNotFoundIDException{
        Role byId = roleRepository.findById(id).orElseThrow(() -> new RoleNotFoundIDException("Role id must be null"));
        return RoleMapper.INSTANCE.roleToDTO(byId);
    }
}
