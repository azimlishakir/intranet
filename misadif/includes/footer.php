<style>
    .footer{
       margin-top: 50px;
    }
</style>

<section class='footer'>
    <div class='container'>
        <div class='row'>
            <div class='col-sm-12'>
                <div class='main_footer'>
                    <div class='row'>

                        <div class='col-sm-6 col-xs-12'>
                            <div class='copyright_text'>
                                <p class=' wow fadeInRight' data-wow-duration='1s'>© Əmanətlərin Sığortalanması Fondunun Mərkəzləşdirilmiş İnformasiya Sistemi
                                    <a href='http://mail.adif.az' target='_blank'>
                                                <span style='float: right; padding-left:20px; padding-top: 20px;'>
                                                    <img src='../public/img/mail.jpg' alt='HelpDesk' height='32' width='50'>
                                                </span>
                                        <a href='http://helpdesk.adif.local/index.php' target='_blank'>
                                                    <span style='float: right; padding-left:20px;'>
                                                        <img src='../public/img/hpdesk.jpg' alt='HelpDesk' height='51' width='50'>
                                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>









</div>

<!-- START SCROLL TO TOP  -->

<div class='scrollup'>
    <a href='#'>
        <i class='fa fa-chevron-up'></i>
    </a>
</div>

<script src='../public/assets/js/vendor/jquery-1.11.2.min.js'></script>
<script src='../public/assets/js/vendor/bootstrap.min.js'></script>

<script src='../public/assets/js/jquery.magnific-popup.js'></script>
<script src='../public/assets/js/jquery.mixitup.min.js'></script>
<script src='../public/assets/js/jquery.easing.1.3.js'></script>
<script src='../public/assets/js/jquery.masonry.min.js'></script>
<script src='../public/assets/js/jquery.fancybox.pack.js'></script>
<script src='../public/assets/plugins/bootstrap-typeahead.min.js'></script>
<script src='../public/assets/js/search.js'></script>

<script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>
<script src='http://maps.google.com/maps/api/js'></script>
<script src='../public/assets/js/gmaps.min.js'></script>


<script>

    function showmap() {
        var mapOptions = {
            zoom: 8,
            scrollwheel: false,
            center: new google.maps.LatLng(-34.397, 150.644),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
    }
</script>

<script src='../public/assets/js/plugins.js'></script>
<script src='../public/assets/js/main.js'></script>
<script src='../public/assets/js/jquery.dataTables.min.js'></script>
<script src='../public/assets/js/jquery.collapser.js'></script>

</body>

</html>