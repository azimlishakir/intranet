$(document).ready(function() {
  var persons = new Array();
  var person = new Array();
  var input = $("#searchform");
  input.typeahead({
    ajax: {
      url: "/doc/data.json",
      preProcess: function(data) {
        toggleLoading();
        if (data.success === false) {
          return false;
        }
        $.each(data, function(index, value) {
          persons[value.id] = value;
        });
        return data;
      },
      preDispatch: function (query) {
        toggleLoading();
        setTimeout(function() {
            return {
                search: query
            }
        },1000);
    },
    },
    triggerLength: 3,
    scrollBar: true,
    displayField: "asa",
    menu: ' <ul class="typeahead list-group dropdown-menu"></ul>',
    item: '<li class=""list-group-item"><a href="#"></a></li>',
    loadingClass: "loading-circle",
    onSelect: function(item) {
        input.val('');
        person = persons[item.value];
        var result =
            '<ul class="list-group">' +
            '<li class="list-group-item">SAA: <span>' + checkValue('asa') + "</span></li>"+
            '<li class="list-group-item">Bank: <span>' + checkValue('bank') + "</span></li>"+
            '<li class="list-group-item">Şöbə: <span>'+ checkValue('section') +"</span></li>"+
            '<li class="list-group-item">Vəzifə: <span>'+ checkValue('position') +"</span></li>"+
            '<li class="list-group-item">Daxili nömrələr: <span>'+ checkValue('innercode') +"</span></li>"+
            '<li class="list-group-item">Email: <span>'+ checkValue('email') +"</span></li>"+
            "</ul>";
        $("#searchres").html(result);
    }
  });

function checkValue(value) {
    return person.hasOwnProperty(value) ? person[value] : 'Göstərilməyib';
}

function toggleLoading() {
    $('#searchloading').toggleClass('hidden')
}



$('#inner-tabs').find('.tab-toggler').click(function(e) {
	e.preventDefault();
	var tab = $('#inner-tabs');
	var activeTab = '.content-tab__' + $(this).data('tab');
	tab.find('.content-tab.active').removeClass('active');
	tab.find(activeTab).addClass('active');
	
});

$('#inner-tabs2').find('.tab-toggler').click(function(e) {
	e.preventDefault();
	var tab = $('#inner-tabs2');
	var activeTab = '.content-tab__' + $(this).data('tab');
	tab.find('.content-tab.active').removeClass('active');
	tab.find(activeTab).addClass('active');
	
});

$('#inner-tabs3').find('.tab-toggler').click(function(e) {
	e.preventDefault();
	var tab = $('#inner-tabs3');
	var activeTab = '.content-tab__' + $(this).data('tab');
	tab.find('.content-tab.active').removeClass('active');
	tab.find(activeTab).addClass('active');
	
});

$('#inner-tabs4').find('.tab-toggler').click(function(e) {
	e.preventDefault();
	var tab = $('#inner-tabs4');
	var activeTab = '.content-tab__' + $(this).data('tab');
	tab.find('.content-tab.active').removeClass('active');
	tab.find(activeTab).addClass('active');
	
});

$('#inner-tabs5').find('.tab-toggler').click(function(e) {
	e.preventDefault();
	var tab = $('#inner-tabs5');
	var activeTab = '.content-tab__' + $(this).data('tab');
	tab.find('.content-tab.active').removeClass('active');
	tab.find(activeTab).addClass('active');
	
});

$('#inner-tabs6').find('.tab-toggler').click(function(e) {
	e.preventDefault();
	var tab = $('#inner-tabs6');
	var activeTab = '.content-tab__' + $(this).data('tab');
	tab.find('.content-tab.active').removeClass('active');
	tab.find(activeTab).addClass('active');
	
});







});