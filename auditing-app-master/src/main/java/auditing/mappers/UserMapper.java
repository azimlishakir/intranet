package auditing.mappers;

import auditing.dto.UsersDTO;
import auditing.model.Users;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;



@Mapper(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
@Component
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mappings({
            @Mapping(target = "id", source = "users.id"),
            @Mapping(target = "name", source = "users.name"),
            @Mapping(target = "lastname", source = "users.lastname"),
            @Mapping(target = "username", source = "users.username"),
            @Mapping(target = "email",source = "users.email"),
            @Mapping(target = "password",source = "users.password"),
            @Mapping(target = "role.id", source = "users.role.id"),
            @Mapping(target = "role.name",source = "users.role.name"),
            @Mapping(target = "role.aUserCollection",ignore = true)
    })
    UsersDTO toDTO (Users users);

    @Mappings({
            @Mapping(target = "id", source = "usersDTO.id"),
            @Mapping(target = "name", source = "usersDTO.name"),
            @Mapping(target = "lastname", source = "usersDTO.lastname"),
            @Mapping(target = "username", source = "usersDTO.username"),
            @Mapping(target = "email",source = "usersDTO.email"),
            @Mapping(target = "password",source = "usersDTO.password"),
            @Mapping(target = "role.id", source = "usersDTO.role.id"),
            @Mapping(target = "role.name",source = "usersDTO.role.name")
    })
    Users users (UsersDTO usersDTO);


}
