<?php
@session_start();
require_once "../db/db.php";
require_once "./db/config.php";
$email=@$_SESSION["email"];
$pass=@$_SESSION["password"];
$departament=$db->prepare("SELECT * FROM departament");
$departament->execute();
$deprows=$departament->rowCount();

if(!empty($email)) {
    echo "

<div class='pull-left col-md-12 admin_top' style='margin-top: 30px'>
    <div class='col-md-12 pull-left'>
        <button class='btn btn-primary pull-left' type='button'>
                Departamentler <span class='badge badge-light'>$deprows</span>
        </button>
 <a href='".SITE_URL."/?folder=dep&page=add' class='btn pull-right btn-success add'><i class='fas fa-plus'></i> Əlavə et</i></a>

    </div>

";

    echo "
        <table class='table table-bordered table-responsive table-hover table-striped' style='margin-top: 30px'>
            <thead>
                <tr>
                    <th>ID</th>
                    <th width='500'>Code</th>
                    <th width='350'>Title</th>
                    <!--<th width='233'>status</th>-->
                    <th>Əməliyyat</th>                    
                </tr>
            </thead>

";
    $sec = $db->prepare("SELECT * FROM departament");
    $sec->execute();
    $res = $sec->fetchAll(PDO::FETCH_ASSOC);
    foreach ($res as $okey) {
        $depid = $okey["id"];
        $deptitle = $okey["title"];
        $depcode = $okey["kod"];
       // $depstatus = $okey["row_status"];
        echo "
            <tbody>
                <tr>
                
                    <td>$depid</td>
                    <td>$depcode</td>
                    <td>$deptitle</td>
                   <!-- <td>1</td> -->
                    <td>
                       
                        <a href='".SITE_URL."/?folder=dep&page=edit&id=$depid' class='btn btn-success'><i class='fas fa-edit'></i> Yenile </a>
                        <a href='".SITE_URL."/?folder=dep&page=question_dep&id=$depid' class='btn btn-danger'><i class='fas fa-trash-alt'></i> Sil </a>
                    </td>
                   
                </tr>
            </tbody>
            ";
    }
    echo "
            
        </table>


</div>
    
    ";
 }
else{
    header("location:" . SITE_URL . "/login.php");
}
?>