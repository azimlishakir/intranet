package auditing.service.impl;

import auditing.dto.RoleDTO;
import auditing.dto.UsersDTO;
import auditing.exception.RoleNotFoundException;
import auditing.exception.RoleNotFoundIDException;
import auditing.model.Role;
import auditing.repository.RoleRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WithMockUser
@SpringBootTest
@RunWith(SpringRunner.class)
public class RoleServiceImplTest {

    @Mock
    private RoleRepository roleRepository;

    @InjectMocks
    private RoleServiceImpl roleService;


    private Role role;

    private RoleDTO roleDTO;

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    private static final long ROLE_ID = 1;
    private static final String ROLE_NAME= "ADMINISTRATOR";

    @BeforeEach
    public void setUp() {
        System.out.println("init");
        roleDTO = RoleDTO.builder().
                id(ROLE_ID).
                name(ROLE_NAME).build();
    }



    @Test
    public void listRole(){
        Role role = new Role(ROLE_ID,ROLE_NAME);
        when(roleRepository.findAll()).thenReturn(Arrays.asList(role));
        List<Role> list = roleService.listRole();
        assertEquals(1,list.size());
        verify(roleRepository).findAll();
    }

    @Test
    public void getRoleSuccess() {
        Role role = new Role(1L,"ADMINISTRATOR");
        when(roleRepository.findById(ROLE_ID)).thenReturn(Optional.of(role));
        RoleDTO list = roleService.getRole(1L);
        assertEquals(role.getId(),list.getId());
        assertEquals(role.getName(),list.getName());
        assertNotEquals(role,roleDTO);
        assertNotNull(role);
        assertNotNull(list);
        verify(roleRepository).findById(ROLE_ID);
    }

    @Test
    public void getRoleExseption(){
        when(roleRepository.findById(ROLE_ID)).thenReturn(Optional.empty());
        exceptionRule.expect(RoleNotFoundException.class);
        exceptionRule.expectMessage("Role not found with name ");
        roleService.getRole(ROLE_ID);
        verify(roleRepository).findById(ROLE_ID);
    }

    @Test
    public void createRoleSuccessTest(){
        Role role = new Role(ROLE_NAME);
        RoleDTO expected = new RoleDTO(ROLE_NAME);
        when(roleRepository.findById(ROLE_ID)).thenReturn(Optional.of(role));
        when(roleRepository.save(any())).thenReturn(role);
        assertEquals(role.getId(),expected.getId());
        assertEquals(role.getName(),expected.getName());
        assertEquals(expected,roleService.createRole(expected));
    }

    @Test
    public void createRoleExceptionTest(){
        RoleDTO roleDTO = RoleDTO.builder().
                id(ROLE_ID)
                .name(ROLE_NAME)
                .build();

        exceptionRule.expect(RoleNotFoundIDException.class);
        exceptionRule.expectMessage("Role id must be null");
        roleService.createRole(roleDTO);
    }

    @Test
    public void updateRoleTest(){
        Role role = new Role(1L,"ADMINISTRATOR");
        RoleDTO roleDTO = RoleDTO.builder().
                id(ROLE_ID)
                .name(ROLE_NAME)
                .build();
        when(roleRepository.findById(roleDTO.getId())).thenReturn(Optional.of(role));
        when(roleRepository.save(any())).thenReturn(role);
        RoleDTO actual = roleService.updateRole(roleDTO,ROLE_ID);
        assertEquals(roleDTO.getId(),actual.getId());
    }

    @Test
    public void deleteUsersTest(){
        Role role = new Role(1L,"ADMINISTRATOR");
        RoleDTO roleDTO = new RoleDTO(1L,"ADMINISTRATOR");
        when(roleRepository.findById(ROLE_ID)).thenReturn(Optional.of(role));
        roleService.deleteRole(ROLE_ID);
        assertEquals(roleDTO,roleService.deleteRole(role.getId()));
    }

    @Test
    public void deleteUsersErrorTest(){
        Role role = new Role(1L,"ADMINISTRATOR");
        RoleDTO roleDTO = new RoleDTO(1L,"ADMINISTRATOR");
        when(roleRepository.findById(ROLE_ID)).thenReturn(Optional.empty());
        exceptionRule.expect(RoleNotFoundIDException.class);
        exceptionRule.expectMessage("Role id must be null");
        assertEquals(roleDTO,roleService.deleteRole(role.getId()));
    }


}