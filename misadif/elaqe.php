<?php include_once "./includes/header.php"; ?>


<section id='home' class='home'>
    <div class='overlay'>
        <div class='container'>
            <div class='row'>
                <div class='col-sm-12 '>
                    <div class='main_home_slider text-center'>
                        <div class='single_home_slider'>
                            <div class='main_home wow fadeInUp' data-wow-duration='700ms'>
                                <h1>HELPDESK MÜRACİƏT</h1>
                                <p>Komputerlərinizdə yaranmış problemlərin həlli üçün müraciət forması</p>
                            </div>
                        </div>
                        <div class='single_home_slider'>
                            <div class='main_home wow fadeInUp' data-wow-duration='700ms'>
                                <h1>XİDMƏTLƏR</h1>
                                <p>Sayta daxil olmaqla aşağıdakı xidmətlərdən yararlana bilərsiniz</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>





<div class='container' style='margin-top: 25px; margin-bottom: 25px; font-family: arial; color: #1a2a68;'>
    <div class='row'>
        <div class='col-lg-6' style='font-family: arial;'>
            <div class='recent'>
                <button class='btn-primarys'><h3>E-maİl yaz</h3></button>
            </div>	<br>
            <form role='form'>
                <div class='form-group'  style='font-family: arial; color: #1a2a68;'>

                    <input type='email' class='form-control' id='exampleInputEmail1' placeholder='E-mail daxil edin'>
                </div>


                <textarea class='form-control' rows='8'></textarea>
                <button type='submit' class='btn btn-default' style='font-family: arial;'>Göndər</button>
            </form>
        </div>

        <div class='col-lg-6' style='font-family: arial; color: #1a2a68;'>
            <br>
            <div class='contact-area' style='font-family: arial;  color: #1a2a68;'>
                <h4 style='font-family: arial;  color: #1a2a68;'>Sistem inzibatçılığı və proqramlaşdırma şöbəsinin müdiri: </h4>
                <p style='font-family: arial;  color: #1a2a68;'>Orxan Cəfərov - dax. 3015   email: orxan.cafarov@adif.az</p><br>

                <h4 style='font-family: arial;'>İnformasiya texnologiyası şöbəsinin mütəxəssisləri: </h4>
                <p style='font-family: arial;  color: #1a2a68;'>	Rafail Zeynalov - dax. 3009   email: rafail.zeynalov@adif.az<br>
                    Cəmalə Hümmətova - dax. 2021  email: jamala.hummatova@adif.az <br>
                    Musa Səlimov - dax. 4343  email: alimusa.salimov@adif.az<br>
                    Fərid Şirinov - dax. 1105 email: ferid.shirinov@adif.az <br>
                    Pərvin Məmmədov - dax. 2222  email: parvin.mammadov@adif.az<br>
                    Rüfət Mehdiyev - dax. 1279  email: rufat.mehdiyev@adif.az <br>
                    <!--İlnur Rezyapov - dax. 8585<br>--></p>

                <h4 style='font-family: arial;  color: #1a2a68;'>İnformasiya texnologiyası şöbəsinin programistləri: </h4>
                    Ziya  Tağızadə - dax. 1164         email: ziya.taghizada@adif.az<br>
                    Əzimli Şakir-  dax. 2026           email: shakir.azimli@adif.az </br>
                    Taleh Aydınov -  dax. 2050         email: taleh.aydinov@adif.az
                </p>



            </div>
        </div>
    </div>
</div>
	
<?php include_once "./includes/footer.php"; ?>